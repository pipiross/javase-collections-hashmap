# About

We can analyse the source code of the `HashMap` class to obtain information about its internal structure.
Then, in runtime, we can use the reflection to get data from the `HashMap` objects.

Initially, we obtain the hash table that is represented by the array.
This array is stored in the `table` field.

```java
Object[] bins = valueOf(map, "table");
```

The `valueOf` method returns the value of the given field in the given object.

Each element of this array represents a bin that contains the linked list of nodes.
So, we can traverse through all nodes in each bin.

```java
Object node = bin;
while (node != null) {
    ...
    node = valueOf(node, "next");
}
```

At last, we obtain data accordingly to the type of this node.
