package hrytsenko.example;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Objects;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

@UtilityClass
public class HashMaps {

    public static int sizeOf(Map<?, ?> map) {
        return (int) valueOf(map, "size");
    }

    public static int thresholdOf(Map<?, ?> map) {
        return (int) valueOf(map, "threshold");
    }

    public static String dump(Map<?, ?> map) {
        Objects.requireNonNull(map);

        StringBuilder dump = new StringBuilder();
        dump.append(String.format("S: %s, T: %s\n", sizeOf(map), thresholdOf(map)));

        Object[] bins = valueOf(map, "table");
        for (int i = 0; i < bins.length; ++i) {
            dump.append(String.format("%02d", i)).append(dumpBin(bins[i])).append("\n");
        }

        return dump.toString();
    }

    public static String dumpBin(Object bin) {
        StringBuilder dump = new StringBuilder();
        Object node = bin;
        while (node != null) {
            dump.append(dumpNode(node));
            node = valueOf(node, "next");
        }
        return dump.toString();
    }

    public static String dumpNode(Object node) {
        String clazz = node.getClass().getSimpleName();
        switch (clazz) {
        case "Node":
            return String.format(" -> [%s %s:%s]", clazz, valueOf(node, "key"), valueOf(node, "hash"));
        case "Entry ":
            return String.format(" -> [%s %s:%s <%s|%s>]", clazz, valueOf(node, "key"), valueOf(node, "hash"),
                    valueOf(node, "before", "key"), valueOf(node, "after", "key"));
        case "TreeNode":
            return String.format(" -> [%s %s:%s <%s|%s> <%s|%s|%s>]", clazz, valueOf(node, "key"),
                    valueOf(node, "hash"), valueOf(node, "before", "key"), valueOf(node, "after", "key"),
                    valueOf(node, "parent", "key"), valueOf(node, "left", "key"), valueOf(node, "right", "key"));
        default:
            throw new IllegalArgumentException(String.format("Unknown class %s.", clazz));
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T valueOf(Object object, String... fieldsNames) {
        Object current = object;
        for (String field : fieldsNames) {
            current = valueOf(current, field);
            if (current == null) {
                return null;
            }
        }
        return (T) current;
    }

    @SuppressWarnings("unchecked")
    public static <T> T valueOf(Object object, String fieldName) {
        try {
            Field field = findField(object, fieldName);
            field.setAccessible(true);
            return (T) field.get(object);
        } catch (Exception exception) {
            throw new IllegalArgumentException(String.format("Field %s not accessible.", fieldName), exception);
        }
    }

    @SneakyThrows
    private static Field findField(Object object, String fieldName) {
        Class<?> clazz = object.getClass();
        while (!Objects.equals(Object.class, clazz)) {
            try {
                return clazz.getDeclaredField(fieldName);
            } catch (NoSuchFieldException exception) {
                clazz = clazz.getSuperclass();
            }
        }

        throw new NoSuchFieldException(String.format("Field %s not found.", fieldName));
    }

}
