package hrytsenko.example;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HashMapsTest {

    private static final Object ANY = new Object();

    @Test
    public void HashMap_C4_T3() {
        Map<String, Object> map = new HashMap<>(4, 0.75f);
        map.put("A", ANY);
        map.put("B", ANY);
        map.put("E", ANY);

        assertThat(HashMaps.sizeOf(map), is(equalTo(3)));
        assertThat(HashMaps.thresholdOf(map), is(equalTo(3)));
        log.info(HashMaps.dump(map));

        map.put("F", ANY);

        assertThat(HashMaps.sizeOf(map), is(equalTo(4)));
        assertThat(HashMaps.thresholdOf(map), is(equalTo(6)));
        log.info(HashMaps.dump(map));
    }

    @Test
    public void LinkedHashMap_C4_T3() {
        Map<String, Object> map = new LinkedHashMap<>(4, 0.75f);
        map.put("A", ANY);
        map.put("B", ANY);
        map.put("E", ANY);

        assertThat(HashMaps.sizeOf(map), is(equalTo(3)));
        assertThat(HashMaps.thresholdOf(map), is(equalTo(3)));
        log.info(HashMaps.dump(map));

        map.put("F", ANY);

        assertThat(HashMaps.sizeOf(map), is(equalTo(4)));
        assertThat(HashMaps.thresholdOf(map), is(equalTo(6)));
        log.info(HashMaps.dump(map));
    }

}
